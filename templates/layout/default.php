<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'Sistema de estabelecimento';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

        <?= $this->Html->css(['normalize.min', 'milligram.min', 'cake']) ?>

        <?= $this->fetch('meta') ?>

        <link rel="stylesheet"
            href="<?= $this->Url->webroot('template_personalizado/assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?= $this->Url->webroot('css/dataTables.bootstrap4.min.css') ?>">
        <link rel="stylesheet" href="<?= $this->Url->webroot('template_personalizado/assets/css/slick.css') ?>">
        <link rel="stylesheet" href="<?= $this->Url->webroot('template_personalizado/assets/css/style.css') ?>">

        <link rel="stylesheet" href="<?= $this->Url->webroot('css/sweetalert2.min.css') ?>">

        <link rel="stylesheet" href="<?= $this->Url->webroot('css/global.css') ?>">

        <?= $this->fetch('css') ?>
    </head>
    <body>
        <?= $this->element('top'); ?>
        <?= $this->element('content'); ?>
        <?= $this->element('footer'); ?>

        <script src="<?= $this->Url->webroot('template_personalizado/assets/js/jquery.min.js') ?>"></script>
        <script src="<?= $this->Url->webroot('template_personalizado/assets/js/popper.min.js') ?>"></script>
        <script src="<?= $this->Url->webroot('template_personalizado/assets/js/bootstrap.min.js') ?>"></script>

        <script src="<?= $this->Url->webroot('js/jquery.dataTables.min.js') ?>"></script>
        <script src="<?= $this->Url->webroot('js/dataTables.bootstrap4.min.js') ?>"></script>

        <script src="<?= $this->Url->webroot('js/dataTable.js') ?>"></script>

        <script src="<?= $this->Url->webroot('js/sweetalert2.min.js') ?>"></script>

        <script src="<?= $this->Url->webroot('js/jquery.mask.min.js') ?>"></script>

        <script src="<?= $this->Url->webroot('js/global.js') ?>"></script>

        <?= $this->fetch('script') ?>
    </body>
</html>
