<?php
    $cakeDescription = 'Sistema de estabelecimento';
?>

<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?= $cakeDescription ?>:
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

        <?= $this->Html->css(['normalize.min', 'milligram.min', 'cake']) ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>

        <link rel="stylesheet" href="<?= $this->Url->webroot ?>template_personalizado/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= $this->Url->webroot ?>template_personalizado/assets/css/slick.css">
        <link rel="stylesheet" href="<?= $this->Url->webroot ?>template_personalizado/assets/css/style.css">
        <link rel="stylesheet" href="<?= $this->Url->webroot('css/global.css') ?>">
    </head>
    <body>
        <?= $this->element('top'); ?>
        <?= $this->element('contentHome'); ?>
        <?= $this->element('footer'); ?>

        <script src="<?= $this->Url->webroot ?>template_personalizado/assets/js/jquery.min.js"></script>
        <script src="<?= $this->Url->webroot ?>template_personalizado/assets/js/popper.min.js"></script>
        <script src="<?= $this->Url->webroot ?>template_personalizado/assets/js/bootstrap.min.js"></script>

        <?= $this->fetch('script') ?>
    </body>
</html>
