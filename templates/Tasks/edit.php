<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Task $task
 */
?>

<div style="padding-bottom: 500px;">
    <div class="column-responsive">
        <?= $this->Form->create($task) ?>
            <fieldset>
                <legend>
                    <h1 style="color: #450b78; font-weight: 700;">Atualizar Tarefa</h1>
                </legend>
                <?php
                    echo $this->Form->control('nome', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('descricao', ['label' => [ 'class' => 'label-color',
                        'text' => 'Descrição' ] ]);

                    echo $this->Form->control('prazo', ['label' => [ 'class' => 'label-color']
                    ]);

                    echo $this->Form->control('prioridade', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('concluida', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('client_id', ['options' => $clients,
                        'label' => [ 'class' => 'label-color', 'text' => 'Cliente' ]]);
                ?>
            </fieldset>

            <?= $this->Form->button('Atualizar', ['style' => 'margin-top: 20px;',
                'class' => 'btn-actions radius-btn btn-add']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
