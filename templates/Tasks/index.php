<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Task[]|\Cake\Collection\CollectionInterface $tasks
 */
?>
<div id="divIndexTasks">
    <?= $this->Flash->render('success'); ?>

    <?= $this->Html->link('Adicionar', ['action' => 'add'],
        ['class' => 'btn-actions radius-btn btn-add float-right']) ?>

    <h1 style="color: #450b78; font-weight: 700;">Tarefas</h1>

    <div class="table-responsive" style="padding-top: 20px;">
        <table class="table table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>
                    <th class="text-center"><?= $this->Paginator->sort('id') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('nome') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('descricao', 'Descrição') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('prazo') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('prioridade') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('concluida') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('client_id', 'Cliente') ?></th>
                    <th class="text-center">Ações</th>
                </tr>
            </thead>
            <tbody id="tbodyTasks">
                <?php foreach ($tasks as $task): ?>

                <tr id="trTask<?= $task->id ?>">
                    <td class="size-text"><?= $this->Number->format($task->id) ?></td>

                    <td class="size-text">
                        <?= h($this->Text->truncate($task->nome, 14, ['ellipsis' => '...', 'exact' => false])); ?>
                    </td>

                    <td class="size-text">
                        <?= h($this->Text->truncate($task->descricao, 14, ['ellipsis' => '...', 'exact' => false])); ?>
                    </td>

                    <td class="size-text"><?= h($task->prazo->i18nFormat('dd/MM/yyyy h:mm:ss')) ?></td>
                    <td class="size-text"><?= h($task->prioridade) ?></td>

                    <?php if ($task->concluida == 0): ?>
                        <td class="size-text">Não</td>
                    <?php elseif($task->concluida == 1): ?>
                        <td class="size-text">Sim</td>
                    <?php endif; ?>

                    <td>
                        <?= $task->has('client') ? $this->Html->link(
                            $this->Text->truncate($task->client->nome, 9, ['ellipsis' => '...', 'exact' => false]),
                                ['controller' => 'Clients', 'action' => 'view', $task->client->id],
                                ['class' => 'btn-actions radius-btn', 'style' => 'background: #292e41']) :
                                'Sem Cliente';
                        ?>
                    </td>

                    <td style="width:30%">
                        <?= $this->Html->link('Visualizar', ['action' => 'view', $task->id],
                            ['class' => 'btn-actions radius-btn']) ?>

                        <?= $this->Html->link('Editar', ['action' => 'edit', $task->id],
                            ['class' => 'btn-actions radius-btn btn-edit']) ?>

                        <a class="btn-actions radius-btn btn-remove" style="cursor: pointer"
                            onclick="removeIndexTask(<?= json_encode($task->id) ?>)">
                            Remover
                        </a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator" style="padding-top: 20px;">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primeiro')) ?>
                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('Próximo') . ' >') ?>
                <?= $this->Paginator->last(__('Último') . ' >>') ?>
            </ul>
        </div>
    </div>
</div>
