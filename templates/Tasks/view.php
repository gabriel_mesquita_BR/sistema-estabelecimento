<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Task $task
 */
?>

<div style="padding-bottom: 500px;">
    <div class="column-responsive">

        <h1 style="color: #450b78; font-weight: 700;"><?= h($task->nome) ?></h1>

        <table class="table table-striped table-bordered" style="width:100%; margin-top: 20px;">
            <tr>
                <th class="text-center">Id</th>
                <td class="text-center size-text"><?= $this->Number->format($task->id) ?></td>
            </tr>
            <tr>
                <th class="text-center">Nome</th>
                <td class="text-center size-text"><?= h($task->nome) ?></td>
            </tr>
            <tr>
                <th class="text-center">Descrição</th>
                <td class="text-center size-text"><?= h($task->descricao) ?></td>
            </tr>
            <tr>
                <th class="text-center">Prazo</th>
                <td class="text-center size-text"><?= h($task->prazo->i18nFormat('dd/MM/yyyy h:mm:ss')) ?></td>
            </tr>
            <tr>
                <th class="text-center">Prioridade</th>
                <td class="text-center size-text"><?= h($task->prioridade) ?></td>
            </tr>

            <tr>
                <th class="text-center">Concluída</th>
                <?php if ($task->concluida == 0): ?>
                    <td class="text-center size-text">Não</td>
                <?php elseif($task->concluida == 1): ?>
                    <td class="text-center size-text">Sim</td>
                <?php endif; ?>
            </tr>

            <tr>
                <th class="text-center">Cliente</th>
                <td class="text-center">
                    <?= $task->has('client') ? $this->Html->link($task->client->nome,
                            ['controller' => 'Clients', 'action' => 'view', $task->client->id],
                            ['class' => 'btn-actions radius-btn', 'style' => 'background: #292e41']) : '' ?>
                </td>
            </tr>

            <tr>
                <th class="text-center">Ações</th>
                <td class="text-center">
                    <?= $this->Html->link('Editar', ['action' => 'edit', $task->id],
                        ['class' => 'btn-actions radius-btn btn-edit']) ?>

                    <a class="btn-actions radius-btn btn-remove" style="cursor: pointer"
                        onclick="removeTask(<?= json_encode($task->id) ?>)">
                        Remover
                    </a>
                </td>
            </tr>
        </table>
    </div>
</div>
