<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>
<div style="padding-bottom: 500px;">
    <div class="column-responsive">

        <?= $this->Flash->render('error'); ?>

        <?= $this->Form->create($client, ['type' => 'file']) ?>
            <fieldset>
                <legend>
                    <h1 style="color: #450b78; font-weight: 700;">Adicionar Cliente</h1>
                </legend>
                <?php
                    echo $this->Form->control('nome', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('email', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('endereco', ['label' => [ 'class' => 'label-color',
                        'text' => 'Endereço']
                    ]);

                    echo $this->Form->control('telefone', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('image', ['type' => 'file', 'label' => [ 'class' => 'label-color',
                        'text' => 'Imagem'], 'class' => 'btn-actions radius-btn btn-input-file' ]);
                ?>
            </fieldset>

            <?= $this->Form->button('Cadastrar', ['style' => 'margin-top: 20px;',
                'class' => 'btn-actions radius-btn btn-add']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
