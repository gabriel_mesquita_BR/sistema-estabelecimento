<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client[]|\Cake\Collection\CollectionInterface $clients
 */
?>
<div id="divIndexClients">

    <?= $this->Flash->render('success'); ?>

    <?= $this->Html->link('Baixar PDF', ['action' => 'pdf' ],
        ['class' => 'btn-actions radius-btn btn-remove float-right']) ?>

    <?= $this->Html->link('Adicionar', ['action' => 'add'],
        ['class' => 'btn-actions radius-btn btn-add float-right', 'style' => 'margin-right: 20px;']) ?>

    <h1 style="color: #450b78; font-weight: 700;">Clientes</h1>

    <div class="table-responsive" style="padding-top: 20px;">
        <table class="table table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>
                    <th class="text-center"><?= $this->Paginator->sort('id') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('nome') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('email') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('endereco', 'Endereço') ?></th>
                    <th class="text-center"><?= $this->Paginator->sort('telefone') ?></th>
                    <th class="text-center">Ações</th>
                </tr>
            </thead>
            <tbody id="tbodyClients">
                <?php foreach ($clients as $client): ?>
                <tr id="trClient<?= $client->id ?>">
                    <td class="size-text"><?= $this->Number->format($client->id) ?></td>

                    <td class="size-text">
                        <?= h($this->Text->truncate($client->nome, 25, ['ellipsis' => '...', 'exact' => false])); ?>
                    </td>

                    <td class="size-text">
                        <?= h($this->Text->truncate($client->email, 25, ['ellipsis' => '...', 'exact' => false])); ?>
                    </td>

                    <td class="size-text"><?= h($client->endereco) ?></td>
                    <td style="width:15%;" class="size-text"><?= h($client->telefone) ?></td>
                    <td style="width:30%;">
                        <?= $this->Html->link('Visualizar', ['action' => 'view', $client->id],
                            ['class' => 'btn-actions radius-btn']) ?>
                        <?= $this->Html->link('Editar', ['action' => 'edit', $client->id],
                            ['class' => 'btn-actions radius-btn btn-edit']) ?>

                        <!-- <= $this->Form->postLink('Remover', ['action' => 'delete', $client->id],
                            ['class' => 'btn-actions radius-btn btn-remove',
                                'onclick' => __('Are you sure you want to delete # {0}?', $client->id)]) ?> -->

                        <a class="btn-actions radius-btn btn-remove" style="cursor: pointer"
                            onclick="removeIndexClient(<?= json_encode($client->id) ?>)">
                            Remover
                        </a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator" style="padding-top: 20px;">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primeiro')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Próximo') . ' >') ?>
            <?= $this->Paginator->last(__('Último') . ' >>') ?>
        </ul>
    </div>
</div>
