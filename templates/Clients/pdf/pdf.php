<!DOCTYPE html>
<html>
    <head>
        <style>
            table, td, th {
                border: 1px solid #ddd;
                text-align: left;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                padding: 5px;
                text-align: center;
            }

            h1 {
                color: #450b78;
                font-weight: 700;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <h1>Clientes</h1>

        <div>
            <table>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Endereço</th>
                        <th>Telefone</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($clients as $client): ?>
                        <tr>
                            <td><?= $client->id; ?></td>
                            <td><?= $client->nome; ?></td>
                            <td><?= $client->email; ?></td>
                            <td><?= $client->endereco; ?></td>
                            <td><?= $client->telefone; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
