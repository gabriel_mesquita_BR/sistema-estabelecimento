<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>
<div style="padding-bottom: 500px;">
    <div class="column-responsive">

        <?php if(file_exists(WWW_ROOT . 'img' . DS . $client->image)): ?>
            <div class="divImage">
                <?= $this->Html->image($this->Url->webroot('img/' . $client->image)); ?>
            </div>
        <?php endif; ?>

        <h1 style="color: #450b78; font-weight: 700;"><?= h($client->nome) ?></h1>

        <table class="table table-striped table-bordered" style="width:100%; margin-top: 20px;">
            <tr>
                <th class="text-center">Id</th>
                <td class="text-center size-text"><?= $this->Number->format($client->id) ?></td>
            </tr>
            <tr>
                <th class="text-center">Nome</th>
                <td class="text-center size-text"><?= h($client->nome) ?></td>
            </tr>
            <tr>
                <th class="text-center">Email</th>
                <td class="text-center size-text"><?= h($client->email) ?></td>
            </tr>
            <tr>
                <th class="text-center">Endereço</th>
                <td class="text-center size-text"><?= h($client->endereco) ?></td>
            </tr>
            <tr>
                <th class="text-center">Telefone</th>
                <td class="text-center size-text"><?= h($client->telefone) ?></td>
            </tr>
            <tr>
                <th class="text-center">Ações</th>
                <td class="text-center">

                    <?= $this->Html->link('Editar', ['action' => 'edit', $client->id],
                            ['class' => 'btn-actions radius-btn btn-edit']) ?>

                    <a class="btn-actions radius-btn btn-remove" style="cursor: pointer"
                        onclick="removeClient(<?= json_encode($client->id) ?>)">
                        Remover
                    </a>
                </td>
            </tr>
        </table>

        <div class="related" style="margin-top: 40px;">
            <h2 style="color: #450b78; font-weight: 700;">Tarefas Relacioadas</h2>

            <?php if (!empty($client->tasks)) : ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" style="width:100%; margin-top: 20px;">
                        <tr>
                            <th>Id</th>
                            <th>Tarefa</th>
                            <th>Descrição</th>
                            <th>Prazo</th>
                            <th>Prioridade</th>
                            <th>Concluída</th>
                            <th>Ações</th>
                        </tr>

                        <?php foreach ($tasks as $task) : ?>
                            <tr>
                                <td class="size-text"><?= h($task->id) ?></td>
                                <td class="size-text"><?= h($task->nome) ?></td>
                                <td class="size-text"><?= h($task->descricao) ?></td>
                                <td class="size-text">
                                    <?= h($task->prazo->i18nFormat('dd/MM/yyyy h:mm:ss')) ?>
                                </td>
                                <td class="size-text"><?= h($task->prioridade) ?></td>

                                <?php if ($task->concluida == 0): ?>
                                    <td class="size-text">Não</td>
                                <?php elseif($task->concluida == 1): ?>
                                    <td class="size-text">Sim</td>
                                <?php endif; ?>

                                <td>
                                    <?= $this->Html->link('Visualizar', ['controller' => 'Tasks',
                                        'action' => 'view', $task->id], ['class' => 'btn-actions radius-btn']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            <?php endif; ?>
        </div>

        <div class="paginator" style="padding-top: 20px;">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primeiro')) ?>
                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('Próximo') . ' >') ?>
                <?= $this->Paginator->last(__('Último') . ' >>') ?>
            </ul>
        </div>
    </div>
</div>
