<main>
    <div class="slider-height slider-padding sky-blue d-flex align-items-center">
        <div class="container">

            <?= $this->Flash->render('adminError'); ?>
            <?= $this->Flash->render('success'); ?>

            <div class="row d-flex align-items-center">

                <div class="col-lg-6 col-md-9 ">
                    <div class="hero__caption">
                        <h1 data-animation="fadeInUp" data-delay=".6s">Get things done<br>with Appco</h1>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="d-lg-block f-right" data-animation="fadeInRight" data-delay="1s">
                        <img src="<?= $this->Url->webroot ?>template_personalizado/assets/img/hero_right.png"
                            alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
