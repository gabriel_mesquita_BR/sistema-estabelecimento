<main>
    <div class="slider-height slider-padding sky-blue d-flex align-items-center">
        <div class="container">
            <?= $this->fetch('content') ?>
        </div>
    </div>
</main>
