<header>
    <div class="header-area header-transparrent ">
        <div class="main-header header-sticky">
            <div class="container">
                <div class="row align-items-center">

                    <div class="col-xl-2 col-lg-2 col-md-2">
                        <div class="logo">
                            <a href="<?= $this->Url->build('/') ?>">
                                <img src="<?= $this->Url->webroot('template_personalizado/assets/img/logo.png') ?>"
                                    alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col-xl-10 col-lg-10 col-md-10">
                        <div class="main-menu f-right d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li>
                                        <a href="<?= $this->Url->build('/') ?>" class="menu">
                                            Home
                                        </a>
                                    </li>

                                    <?php if($this->Identity->isLoggedIn() == true): ?>
                                        <li>
                                            <a href="<?= $this->Url->build(['controller' => 'clients',
                                                'action' => 'index']) ?>" class="menu">
                                                Clientes
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?= $this->Url->build(['controller' => 'tasks',
                                                'action' => 'index']) ?>" class="menu">
                                                Tarefas
                                            </a>
                                        </li>

                                        <li><a href="javascript:void(0)">Perfil</a>
                                            <ul class="submenu">
                                                <li>
                                                    <a href="<?= $this->Url->build(['controller' => 'admins',
                                                        'action' => 'view', $this->Identity->getId()]) ?>">
                                                        Visualizar
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a href="<?= $this->Url->build(['controller' => 'admins',
                                                'action' => 'logout']) ?>" class="menu">
                                                Logout
                                            </a>
                                        </li>

                                    <?php else: ?>

                                        <li>
                                            <a href="<?= $this->Url->build(['controller' => 'admins',
                                                'action' => 'add']) ?>" class="menu">
                                                Criar Conta
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?= $this->Url->build(['controller' => 'admins',
                                                'action' => 'login']) ?>" class="menu">
                                                Login
                                            </a>
                                        </li>

                                    <?php endif; ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
