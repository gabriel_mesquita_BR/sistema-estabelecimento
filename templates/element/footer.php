<footer>
    <div class="container">
        <div class="text-center">
            <p style="color: #450b78">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script>
            </p>
        </div>
    </div>
</footer>
