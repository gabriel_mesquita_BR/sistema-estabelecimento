<div style="padding-bottom: 500px;">
    <div class="column-responsive">

        <?= $this->Flash->render('adminError'); ?>

        <?= $this->Form->create($admin) ?>
            <fieldset>
                <legend>
                    <h1 style="color: #450b78; font-weight: 700;">Adicionar Administrador</h1>
                </legend>
                <?php
                    echo $this->Form->control('nome', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('email', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('password', ['label' => [ 'class' => 'label-color',
                        'text' => 'Senha']
                    ]);

                    // echo $this->Form->control('senha', ['label' => [ 'class' => 'label-color'],
                    //  'type' => 'password']);
                ?>
            </fieldset>

            <?= $this->Form->button('Cadastrar', ['style' => 'margin-top: 20px;',
                'class' => 'btn-actions radius-btn btn-add']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
