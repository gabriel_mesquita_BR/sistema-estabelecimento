<div style="padding-bottom: 500px;">
    <div class="column-responsive">

        <?= $this->Flash->render('adminSuccess'); ?>

        <?= $this->Flash->render('adminError'); ?>

        <?= $this->Flash->render('adminUpdateSuccess'); ?>

        <?= $this->Form->create() ?>
            <fieldset>
                <legend>
                    <h1 style="color: #450b78; font-weight: 700;">Insira suas credenciais</h1>
                </legend>
                <?php
                    echo $this->Form->control('email', ['label' => [ 'class' => 'label-color' ] ]);

                    echo $this->Form->control('password', ['label' => [ 'class' => 'label-color',
                        'text' => 'Senha']
                    ]);
                ?>
            </fieldset>

            <?= $this->Form->button('Login', ['style' => 'margin-top: 20px;',
                'class' => 'btn-actions radius-btn btn-add']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>
