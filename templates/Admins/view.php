<div style="padding-bottom: 500px;">
    <div class="column-responsive">

        <h1 style="color: #450b78; font-weight: 700;"><?= h($admin->nome) ?></h1>

        <table class="table table-striped table-bordered" style="width:100%; margin-top: 20px;">
            <tr>
                <th class="text-center">Id</th>
                <td class="text-center size-text"><?= $this->Number->format($admin->id) ?></td>
            </tr>
            <tr>
                <th class="text-center">Nome</th>
                <td class="text-center size-text"><?= h($admin->nome) ?></td>
            </tr>
            <tr>
                <th class="text-center">Email</th>
                <td class="text-center size-text"><?= h($admin->email) ?></td>
            </tr>
            <tr>
                <th class="text-center">Ações</th>
                <td class="text-center">
                    <?= $this->Html->link('Editar', ['action' => 'edit', $admin->id],
                        ['class' => 'btn-actions radius-btn btn-edit']) ?>

                    <a class="btn-actions radius-btn btn-remove" style="cursor: pointer"
                        onclick="removeAdmin(<?= json_encode($admin->id) ?>)">
                        Remover
                    </a>
                </td>
            </tr>
        </table>
    </div>
</div>
