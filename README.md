# SISTEMA ESTABELECIMENTO #

## VERSÕES

### PHP 7.2
### CAKEPHP 4.1

## BANCO DE DADOS

### MYSQL

## PASSOS PARA A EXECUÇÃO DO PROJETO

* git clone https://gabriel_mesquita_BR@bitbucket.org/gabriel_mesquita_BR/sistema-estabelecimento.git

* Acesse o diretório sistema_estabelecimento

* composer install

* Na seguinte pergunta: Set Folder Permissions ? (Default to Y) [Y,n]?
* Digite: Y

* Crie um database no MySQL

* No diretório config e arquivo app_local.php defina o username e password do seu database e na key database
  localizado dentro da key 'default' no arquivo app_local.php digite o nome do database que criou

* Exemplo do meu código: 'username' => 'root', 'password' => 'root', 'database' => 'estabelecimento_cake4'

* bin/cake migrations migrate => executa as migrations

* bin/cake migrations seed => executa os dados de testes

* bin/cake server
