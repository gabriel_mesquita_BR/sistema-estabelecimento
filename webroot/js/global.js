$(document).ready(function() {

    $('#telefone').mask('(00) 00000-0000');

    $(".menu").each(function(){
        if ($(this).attr("href") == window.location.pathname){
            $(this).parent().addClass("active");

            addStyleInTableClients();
            addStyleInTableTasks();
        }
    });
});

function addStyleInTableClients() {

    let tbodyClients = $('#tbodyClients');

    if (tbodyClients.children().length < 10) {
        $('#divIndexClients').css('padding-bottom', '500px');
    }else {
        $('#divIndexClients').css('padding-bottom', '0px');
        $('#divIndexClients').css('padding-top', '45px');
    }
}

function addStyleInTableTasks() {

    let tbodyTasks = $('#tbodyTasks');

    if (tbodyTasks.children().length < 10) {
        $('#divIndexTasks').css('padding-bottom', '500px');
    }else {
        $('#divIndexTasks').css('padding-bottom', '0px');
        $('#divIndexTasks').css('padding-top', '45px');
    }
}

function removeIndexClient(idClient) {

    Swal.fire({
        title: 'Você tem certeza?',
        text: "O cliente com id " + idClient + " será removido!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Remover',
        cancelButtonText: 'Cancelar'
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                type: "POST",
                url: `/clients/delete`,
                data: {'id': idClient},
                dataType: 'json',
                success: function(response) {

                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed) {
                            $("#trClient" + idClient).fadeOut(1500, function() {
                                $(this).remove();
                            });
                        }
                    });
                },
                error: function(response) {

                    Swal.fire({
                        icon: 'error',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location.reload();
                    });
                }
            });
        }
    });
}

function removeClient(idClient) {

    Swal.fire({
        title: 'Você tem certeza?',
        text: "O cliente com id " + idClient + " será removido!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Remover',
        cancelButtonText: 'Cancelar'
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                type: "POST",
                url: `/clients/delete`,
                data: {'id': idClient},
                dataType: 'json',
                success: function(response) {

                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location = '/clients';
                    });
                },
                error: function(response) {

                    Swal.fire({
                        icon: 'error',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location.reload();
                    });
                }
            });
        }
    });
}

function removeIndexTask(idTask) {

    Swal.fire({
        title: 'Você tem certeza?',
        text: "A tarefa com id " + idTask + " será removida!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Remover',
        cancelButtonText: 'Cancelar'
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                type: "POST",
                url: `/tasks/delete`,
                data: {'id': idTask},
                dataType: 'json',
                success: function(response) {

                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed) {
                            $("#trTask" + idTask).fadeOut(1500, function() {
                                $(this).remove();
                            });
                        }
                    });
                },
                error: function(response) {

                    Swal.fire({
                        icon: 'error',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location.reload();
                    });
                }
            });
        }
    });
}

function removeTask(idTask) {

    Swal.fire({
        title: 'Você tem certeza?',
        text: "A tarefa com id " + idTask + " será removida!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Remover',
        cancelButtonText: 'Cancelar'
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                type: "POST",
                url: `/tasks/delete`,
                data: {'id': idTask},
                dataType: 'json',
                success: function(response) {

                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location = '/tasks';
                    });
                },
                error: function(response) {

                    Swal.fire({
                        icon: 'error',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location.reload();
                    });
                }
            });
        }
    });
}

function removeAdmin(idAdmin) {

    Swal.fire({
        title: 'Você tem certeza?',
        text: "O administrador com id " + idAdmin + " será removido!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Remover',
        cancelButtonText: 'Cancelar'
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                type: "POST",
                url: `/admins/delete`,
                data: {'id': idAdmin},
                dataType: 'json',
                success: function(response) {

                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location = '/admins/logout';
                    });
                },
                error: function(response) {

                    Swal.fire({
                        icon: 'error',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location.reload();
                    });
                }
            });
        }
    });
}
