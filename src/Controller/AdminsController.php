<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Admins Controller
 *
 * @property \App\Model\Table\AdminsTable $Admins
 * @method \App\Model\Entity\Admin[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdminsController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['add']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */

    /**
     * View method
     *
     * @param string|null $id Admin id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $identity = $this->Authentication->getIdentity();
        $adminIdentity = $identity->getOriginalData();

        if($adminIdentity->id == intval($id)) {
            $admin = $this->Admins->get($id);
            $this->set(compact('admin'));
        }else {
            $this->Flash->error('Você não possui autorização para acessar essas informações',
                ['key' => 'adminError']);

            return $this->redirect('/');
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $admin = $this->Admins->newEmptyEntity();

        if ($this->request->is('post')) {

            $admin = $this->Admins->patchEntity($admin, $this->request->getData());

            if ($this->Admins->save($admin)) {

                $this->Flash->success('Administrador salvo com sucesso', ['key' => 'adminSuccess']);

                return $this->redirect(['controller' => 'Admins', 'action' => 'login']);
            }
            $this->Flash->error('Não foi possível salvar o administrador, por favor tente novamente',
                ['key' => 'adminError']);
        }

        $this->set(compact('admin'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Admin id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $identity = $this->Authentication->getIdentity();
        $adminIdentity = $identity->getOriginalData();

        if($adminIdentity->id == intval($id)) {

            // $admin = $this->Admins->get($id);

            $admin = $this->Admins->find()
                        ->selectAllExcept($this->Admins, ['password'])
                        ->where(['id' => $id])
                        ->first();

            if ($this->request->is(['patch', 'post', 'put'])) {

                $admin = $this->Admins->patchEntity($admin, $this->request->getData());

                if ($this->Admins->save($admin)) {

                    $this->Flash->success('Administrador atualizado com sucesso', ['key' => 'adminUpdateSuccess']);

                    return $this->redirect(['controller' => 'Admins', 'action' => 'logout']);
                }
                $this->Flash->error('Administrador não foi atualizado, por favor tente novamente', ['key' => 'adminError']);
            }
            $this->set(compact('admin'));
        }else {
            $this->Flash->error('Você não possui autorização para acessar essas informações',
                ['key' => 'adminError']);

            return $this->redirect('/');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Admin id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $id = $this->request->getData('id');

        $identity = $this->Authentication->getIdentity();
        $adminIdentity = $identity->getOriginalData();

        if($adminIdentity->id == intval($id)) {

            $this->request->allowMethod(['post', 'delete']);

            $admin = $this->Admins->get($id);

            if ($this->Admins->delete($admin)) {
                //$this->Flash->success('Administrador foi removido com sucesso', ['key' => 'success']);
                $this->set(['message' => 'Administrador removido com sucesso']);

            } else {
                //$this->Flash->error('Administrador não foi removido, por favor tente novamente', ['key' => 'error']);
                $this->set(['message' => 'Administrador removido com sucesso']);
            }

            //return $this->redirect(['action' => 'logout']);

            $this->viewBuilder()->setOption('serialize', true);
            $this->RequestHandler->renderAs($this, 'json');
        }else {
            $this->Flash->error('Você não possui autorização para acessar essas informações',
                ['key' => 'adminError']);

            $this->set(['message' => 'Administrador removido com sucesso']);
        }
    }

    public function login()
    {
        $result = $this->Authentication->getResult();

        // If the user is logged in send them away.
        if ($result->isValid()) {

            $identity = $this->Authentication->getIdentity();
            $admin = $identity->getOriginalData();

            $this->Flash->success('Bem vindo ' . $admin->nome, ['key' => 'success']);
            return $this->redirect('/');
        }

        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error('Credenciais Inválidas', ['key' => 'adminError']);
        }
    }

    public function logout()
    {
        $this->Authentication->logout();
        return $this->redirect(['controller' => 'Admins', 'action' => 'login']);
    }
}
