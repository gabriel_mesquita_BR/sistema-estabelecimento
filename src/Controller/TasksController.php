<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Tasks Controller
 *
 * @property \App\Model\Table\TasksTable $Tasks
 * @method \App\Model\Entity\Task[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TasksController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clients'],
        ];
        $tasks = $this->paginate($this->Tasks, ['limit' => '10']);

        $this->set(compact('tasks'));
    }

    /**
     * View method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $task = $this->Tasks->get($id, [
            'contain' => ['Clients'],
        ]);

        $this->set(compact('task'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $task = $this->Tasks->newEmptyEntity();

        if ($this->request->is('post')) {

            $task = $this->Tasks->patchEntity($task, $this->request->getData());

            if ($this->Tasks->save($task)) {

                $this->Flash->success(__('Tarefa salva com sucesso'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('Tarefa não foi salva, por favor tente novamente'));
        }

        $clientsQuery = $this->Tasks->Clients->find()->select(['id', 'nome']);

        $clients = [];

        foreach($clientsQuery as $client) {
            $clients[$client->id] = $client->nome;
        }

        $this->set(compact('task'));
        $this->set('clients', $clients);
    }

    /**
     * Edit method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $task = $this->Tasks->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $task = $this->Tasks->patchEntity($task, $this->request->getData());
            if ($this->Tasks->save($task)) {

                $this->Flash->success(__('Tarefa alterada com sucesso'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('Tarefa não foi alterada com sucesso'));
        }

        $clientsQuery = $this->Tasks->Clients->find()->select(['id', 'nome']);

        $clients = [];

        foreach($clientsQuery as $client) {
            $clients[$client->id] = $client->nome;
        }

        $this->set(compact('task'));
        $this->set('clients', $clients);
    }

    /**
     * Delete method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $id = $this->request->getData('id');

        $this->request->allowMethod(['post', 'delete']);

        $task = $this->Tasks->get($id);

        if ($this->Tasks->delete($task)) {
            // $this->Flash->success(__('Tarefa deletada com sucesso'));
            $this->set(['message' => 'Tarefa removida com sucesso']);

        } else {
            // $this->Flash->error(__('Tarefa não foi deletada, por favor tente novamente'));
            $this->set(['message' => 'Tarefa não foi removida, por favor tente novamente']);
        }

        $this->viewBuilder()->setOption('serialize', true);
        $this->RequestHandler->renderAs($this, 'json');

        // return $this->redirect(['action' => 'index']);
    }
}
