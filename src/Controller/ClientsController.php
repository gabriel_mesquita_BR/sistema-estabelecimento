<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Event\EventInterface;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClientsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $clients = $this->paginate($this->Clients, ['limit' => '10']);

        $this->set(compact('clients'));
    }

    /**
     * View method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $client = $this->Clients->get($id, [
            'contain' => ['Tasks'],
        ]);

        $this->loadModel('Tasks');

        $tasks = $this->Tasks->find()->where(['client_id' => $client->id]);

        $this->set(compact('client'));
        $this->set('tasks', $this->paginate($tasks, ['limit' => '5']));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $client = $this->Clients->newEmptyEntity();

        if ($this->request->is('post')) {

            $client = $this->Clients->patchEntity($client, $this->request->getData());

            if(!$client->getErrors) {

                $image = $this->request->getData('image');

                $image_name = $image->getClientFileName();

                if($image_name) {

                    if(!is_dir(WWW_ROOT . 'img' . DS . 'clients'))
                        mkdir(WWW_ROOT . 'img' . DS . 'clients');

                    $target_path = WWW_ROOT . 'img' . DS . 'clients' . DS . $image_name;

                    $image->moveTo($target_path);

                    $client->image = 'clients/' . $image_name;
                }
            }

            if ($this->Clients->save($client)) {

                $this->Flash->success('Cliente salvo com sucesso', ['key' => 'success']);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('O cliente não foi salvo, por favor tente novamente', ['key' => 'error']);
        }
        $this->set(compact('client'));

    }

    /**
     * Edit method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $client = $this->Clients->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $client = $this->Clients->patchEntity($client, $this->request->getData());

            if(!$client->getErrors) {

                $image = $this->request->getData('change_image');

                $image_name = $image->getClientFileName();

                if($image_name) {

                    if(!is_dir(WWW_ROOT . 'img' . DS . 'clients'))
                        mkdir(WWW_ROOT . 'img' . DS . 'clients', 0775);

                    $target_path = WWW_ROOT . 'img' . DS . 'clients' . DS . $image_name;

                    $image->moveTo($target_path);

                    $imgPath = WWW_ROOT . 'img' . DS . $client->image;

                    if(file_exists($imgPath))
                        unlink($imgPath);

                    $client->image = 'clients/' . $image_name;
                }
            }

            if ($this->Clients->save($client)) {

                $this->Flash->success('Cliente alterado com sucesso', ['key' => 'success']);

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error('O cliente não foi alterado com sucesso, por favor tente novamente',
                ['key' => 'error']);
        }

        $this->set(compact('client'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $id = $this->request->getData('id');

        $this->request->allowMethod(['post', 'delete']);

        $client = $this->Clients->get($id);

        $imgPath = WWW_ROOT . 'img' . DS . $client->image;

        if ($this->Clients->delete($client)) {
            // $this->Flash->success('Cliente deletado com sucesso');

            if(file_exists($imgPath))
                unlink($imgPath);

            $this->set(['message' => 'Cliente removido com sucesso']);

        } else {
            //$this->Flash->error('Cliente não foi deletado, por favor tente novamente');
            $this->set(['message' => 'Cliente não foi removido, por favor tente novamente']);
        }

        $this->viewBuilder()->setOption('serialize', true);
        $this->RequestHandler->renderAs($this, 'json');

        // return $this->redirect(['action' => 'index']);
    }

    public function pdf()
    {
        $this->viewBuilder()->enableAutoLayout(false);
        $clients = $this->paginate($this->Clients);
        $this->viewBuilder()->setClassName('CakePdf.Pdf');

        $this->viewBuilder()->setOption(
            'pdfConfig',
            [
                'orientation' => 'portrait',
                'download' => true,
                'filename' => 'Clientes.pdf'
            ]
        );

        $this->set('clients', $clients);
    }
}
