<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Clients seed.
 */
class ClientsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];

        for ($i = 1; $i <= 100; $i++) {
            $data[] = [
                'nome'      => $faker->userName,
                'email'         => $faker->email,
                'endereco'         => $faker->address,
                'telefone'    => $faker->phoneNumber,
                'image'    => $faker->text,
            ];
        }

        $table = $this->table('clients');
        $table->insert($data)->save();
    }
}
